<?php

require_once 'config.php';


// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Vérification du formulaire est envoyé
if (isset($_POST["inscription"])) {

    // Recupération des utilisateurs
    $sql = "SELECT * FROM user";
    $requete = $db->prepare($sql);
    $requete->execute();

    $users = $requete->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($_POST)) {
        // Vérification des champs requis 
        if (!empty($_POST["email"]) && !empty($_POST["pass"])) {

            // Protection des données
            $pseudo = strip_tags($_POST["surnom"]);

            // Verification de l'email 
            if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                header("location:inscription.php");
            } else {
                $email = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
            }


            // Hacher le mot de passe
            $pass = password_hash($_POST["pass"], PASSWORD_DEFAULT);
            // die ($pass);

            // Si le mail existe déjà
            try {
                $sql = "INSERT INTO user (pseudo, email, password) VALUES (:pseudo, :email, :pass)";
                $requete = $db->prepare($sql);
                if ($requete->execute(["pseudo" => $pseudo, "email" => $email, "pass" => $pass])) {
                }
            } catch (PDOException $error) {
                if ($error->getCode() === '23000') {
                    header('Location: connexion.php');
                }
            }

            header("Location: connexion.php");

            // die($sql);


        } else {
            die("Veuillez compléter");
        }
    }
}

?>

<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main.css">
    <title>Inscription</title>
</head>

<body>

    <!-- Création du formulaire d'inscription, changement de language-->


    <form method="post" action="inscription.php">

        <div class="container1">

            <h1>Inscription</h1>

            <div class="email"> <label for="email"></label>
                <input required type="email" placeholder="Email" name="email" id="email">
            </div>

            <div class="pseudo"> <label for="pseudo"></label>
                <input required type="text" placeholder="Pseudo" name="surnom" id="pseudo">
            </div>

            <div class="pass"> <label for="pass"></label>
                <input required type="password" placeholder="Mot de passe" name="pass" id="pass">
            </div>

            <div class="inscription">
                <input id="inscription" required type="submit" name="inscription" value="M'inscrire">
            </div>

            <div class="existant">
                <a href="connexion.php">Déjà un compte ?</a>
            </div>

        </div>

    </form>

</body>

</html>