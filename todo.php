<?php

require_once 'config.php';
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION["user"])) {
    header("location:connexion.php");
}


// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


if (isset($_POST["ajouter"])) {

    if (!empty($_POST)) {
        // Vérification des champs requis 
        if (!empty($_POST["titre"]) && !empty($_POST["descriptif"]) && !empty($_POST["select"]) && !empty($_POST["datedebut"]) && !empty($_POST["categorie"])) {

            // Protection des données
            $titre = strip_tags($_POST["titre"]);
            $descriptif = strip_tags($_POST["descriptif"]);
            $categorie = strip_tags($_POST["categorie"]);

            // Création de variables
            $select = ($_POST["select"]);
            $datedebut = ($_POST["datedebut"]);
            $datefin = ($_POST["datefin"]);

            // Connexion à l'utilisateur
            $user = $_SESSION["user"];
            $iduser = ($user["id"]);

            //Requete
            $sql = "INSERT INTO todo (id_user, titre, description, status, cree_le, date_limite, categorie) VALUES (:id_user, :titre, :descriptif, :select, :datedebut, :datefin, :categorie)";
            $requete = $db->prepare($sql);
            $requete->execute(["id_user" => $iduser, "titre" => $titre, "descriptif" => $descriptif, "select" => $select, "datedebut" => $datedebut, "datefin" => $datefin, "categorie" => $categorie]);


            header("Location: list.php");
        }
    }
}

?>


<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main.css">
    <title>Todos</title>
</head>

<body>
    <a href="deconnexion.php">deco</a>

    <form action="todo.php" method="POST">

        <div class="container3">

            <h1>To do</h1>

            <div class="titre">
                <input required type="text" placeholder="Titre" name="titre">
            </div>

            <div class="descriptif">
                <textarea required placeholder="Description..." name="descriptif" id="descriptif" cols="30" rows="10"></textarea>
            </div>

            <div class="select">
                <select required name="select" id="select">
                    <option value="A faire">A faire</option>
                    <option value="En cours">En cours</option>
                    <option value="Termine">Terminé</option>
                </select>
            </div>

            <div class="dates">
                <label for="start">Créé le :
                    <input required type="date" value="" name="datedebut">
                </label>

                <label for="end">Pour le :
                    <input type="date" value="" name="datefin">
                </label>
            </div>

            <div class="categorie">
                <input required type="text" placeholder="Catégorie" name="categorie">
            </div>

            <div class="ajouter">
                <input type="submit" value="Ajouter" name="ajouter">
            </div>

            <div class="annuler">
                <a href="list.php" >Annuler</a>
            </div>

        </div>

    </form>

</body>

</html>