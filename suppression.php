<?php

require_once 'config.php';


// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$sql= "DELETE FROM todo WHERE id_todo=:id_todo";

$requete = $db->prepare($sql);

$id = $_GET ["id_todo"];
$requete->bindValue(":id_todo", $id);

$requete->execute();

header('Location: list.php');


