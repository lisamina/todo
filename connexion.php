<?php

require_once 'config.php';

// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Vérification du formulaire est envoyé

if (!empty($_POST["connexion"])) {

    // Vérification des champs requis 
    if (!empty($_POST["email"]) && !empty($_POST["pass"])) {

        // Verification de l'email 
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {

            die("L'adresse email est incorrecte");
        } else {
            $email = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
        }

        // Connexion à la base de données

        $sql = "SELECT * FROM user WHERE email = :email";

        $requete = $db->prepare($sql);
        $requete->bindValue(":email", $email);
        $requete->execute();

        $user = $requete->fetch(PDO::FETCH_BOTH);

        //Si l'utilisateur n'existe pas

        if (!$user) {
            die("L'utilisateur ou le mot de passe est incorrect");
        }

        // Si user existant, vérifier le mot de passe

        if (!password_verify($_POST["pass"], $user["password"])) {
            die("L'utilisateur ou le mot de passe est incorrect");
        } else { 
            session_start();

            // L'utilisateur et le mot de passe sont correctes : On ouvre la session
            // Démarage de la session PHP
            // On stocke dans $_SESSION les informations de l'utilisateur

            $_SESSION["user"] = [
                "pseudo" => $user["pseudo"],
                "email" => $user["email"],
                "id" => $user["id"],
            ];



            // On redirige vers la TODOLIST 

            header("Location: list.php");
        }
    }
}

?>

<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main.css">
    <title>Connexion</title>
</head>

<body>

    <!-- Création du formulaire de connection, changement de language-->


    <form method="post" action="connexion.php">

        <div class="container2">

            <h1>Connexion</h1>

            <div class="email"> <label for="email"></label>
                <input required type="email" placeholder="Email" name="email" id="email">
            </div>

            <div class="pass"> <label for="pass"></label>
                <input required type="password" placeholder="Mot de passe"  name="pass" id="pass">
            </div>

            <div class="connexion">
                <input required id="connexion" type="submit" name="connexion" value="Me connecter">
            </div>

            <div class="nonexistant">
                <a href="inscription.php">Vous n'êtes pas encore inscrit ?</a>
            </div>

        </div>

    </form>

</body>

</html>