USE inscription;


CREATE TABLE IF NOT EXISTS todo (
    id_todo INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    cree_le DATE NOT NULL,
    date_limite DATE,
    status VARCHAR(50) NOT NULL,
    categorie VARCHAR(50) NOT NULL,
    id_user INT,

    FOREIGN KEY (id_user) REFERENCES user(id)

);

