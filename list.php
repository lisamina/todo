<?php

require_once 'config.php';
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION["user"])) {
    header("location:connexion.php");
}

// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Connexion à l'utilisateur
$user = $_SESSION["user"];
$iduser = ($user["id"]);


// Faire requete et l'exécuter 

$sql = "SELECT * FROM todo";
$requete = $db->prepare($sql);
$requete->execute();
$result = $requete->fetchAll(PDO::FETCH_ASSOC);

// Faire une boucle 




?>

<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<!-- boucle et affichage des elements dans html -->


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main.css">
    <title>liste</title>
</head>

<body>

    <div class="container4">
        <h1>Liste</h1>
        <div class="plus">
            <a href="todo.php">Plus</a>
        </div>
        <?php

        // Création boucle foreach
        foreach ($result as $un_resultat) {
        ?>
            <div class="presentation">

                <div class="wrap">

                    <div class="description">
                        <?php echo ($un_resultat["description"]); ?>
                    </div>

                    <div class="select">
                        <?php echo ($un_resultat["status"]); ?>

                    </div>

                    <div class="datedebut">
                        <?php echo ($un_resultat["cree_le"]); ?>
                    </div>

                    <div class="datelimite">
                        <?php echo ($un_resultat["date_limite"]); ?>
                    </div>

                    <div class="categorie">
                        <?php echo ($un_resultat["categorie"]); ?>
                    </div>

                    <div class="fixed">
                        <div class="titre">
                            <?php echo ($un_resultat['titre']); ?>
                        </div>

                        <div class="tool">

                            <div class="modif">
                                <a name="modif" href="modification.php?id_todo=<?php echo ($un_resultat["id_todo"]); ?>">Modifier</a>
                            </div>

                            <div class="supp">
                                <a name="supp" href="suppression.php?id_todo=<?php echo ($un_resultat["id_todo"]); ?>">Supprimer</a>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        <?php

        }; // fin du foreach 
        ?>

        <div class="deco">
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </div>

</body>

</html>