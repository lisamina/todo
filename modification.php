<?php

require_once 'config.php';
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
if (empty($_SESSION["user"])) {
    header("location:connexion.php");
}

// on utilise un bloc try...catch pour tester la connexion et intercepter les erreurs éventuelles
try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : ' . $error->getMessage();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(isset($_GET['id_todo'])) {
    $sql = "SELECT * FROM todo WHERE id_todo= :id_todo";
    
    $requete = $db->prepare($sql);
    
    $id = $_GET["id_todo"];
    $requete->bindValue(":id_todo", $id);
    
    $requete->execute();
    $todo = $requete->fetch();

}






?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="main.css">
    <title>Todos</title>
</head>

<body>
    <a href="deconnexion.php">deco</a>

    <form action="modification.php?id_todo=<?php echo $id ?>" method="POST">

        <div class="container3">

            <h1>To do</h1>

            <div class="titre">
                <input required placeholder="Titre" type="text" name="titre" value="<?php echo ($todo['titre']); ?>">
            </div>

            <div class="descriptif">
                <textarea required placeholder="Description..." name="description" id="descriptif" cols="30" rows="10"><?php echo ($todo['description']); ?></textarea>
            </div>

            <div class="select">
                <select required name="select" id="select"> <?php echo ($todo['status']); ?>
                    <option value="A faire">A faire</option>
                    <option value="En cours">En cours</option>
                    <option value="Terminé">Terminé</option>
                </select>
            </div>

            <div class="dates">
                <label for="start">Créé le :
                    <input required type="date" value="<?php echo ($todo['cree_le']); ?>" name="cree_le">
                </label>

                <label for="end">Pour le :
                    <input type="date" name="date_limite" value="<?php echo ($todo['date_limite']); ?>">
                </label>
            </div>

            <div class="categorie">
                <input required type="text" placeholder="Catégorie" name="categorie" value="<?php echo ($todo['categorie']); ?>">
            </div>


            <div class="ajouter">
                <input type="submit" value="Modifier" name="modifier">
            </div>

            <div class="annuler">
                <a href="list.php" >Annuler</a>
            </div>

        </div>

    </form>

    <?php

if (isset($_POST["modifier"])) {

    $id_user = $_SESSION['user']['id'];
    // $iduser = $_POST["id_user"];
    $titre = $_POST["titre"];
    $descriptif = $_POST["description"];
    $select = $_POST["select"];
    $datedebut = $_POST["cree_le"];
    $datefin = $_POST["date_limite"];
    $categorie = $_POST["categorie"];
    
    $datas = [
        "titre" => $titre,
        "description" => $descriptif,
        "status" => $select,
        "cree_le" => $datedebut,
        "date_limite" => $datefin,
        "categorie" => $categorie,
    ];

   

   try {
    $sql = "UPDATE todo SET titre = ':titre', description = ':descriptif', status = ':select', cree_le = ':datedebut', date_limite = :datefin, 
    categorie = ':categorie', id_user = ':id_user' WHERE id_todo = ':id'";

    $query = $db->prepare($sql);
    
    $query-> bindValue(":titre", $titre, PDO::PARAM_STR);
    $query-> bindValue(":descriptif", $descriptif, PDO::PARAM_STR);
    $query-> bindValue(":select", $select, PDO::PARAM_STR);
    $query-> bindValue(":datedebut", $datedebut, PDO::PARAM_STR);
    $query-> bindValue(":datefin", $datefin, PDO::PARAM_STR);
    $query-> bindValue(":categorie", $categorie, PDO::PARAM_STR);
    $query-> bindValue(":id_user", $id_user, PDO::PARAM_INT);

    $query-> bindValue(":id", $id, PDO::PARAM_INT);
    return $query->execute($datas);

   } catch (\PDOException $e) {
    echo $e->getMessage();


   } 

     //header("location:list.php");
};


    ?>

</body>

</html>