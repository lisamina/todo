CREATE DATABASE IF NOT EXISTS inscription; 

-- Créer un utlisateur MySQL pour cette application 
CREATE USER IF NOT EXISTS 'batman'@'localhost' IDENTIFIED BY 'robin'; 

-- Pour modifier le mot de passe d'un utilisateur MySQL (si on l'a perdu par exemple). A faire depuis le compte root. 

-- Gestion des droits : 
-- On accorde tous les droits à l'utilisateur batman sur la base profile 

GRANT ALL PRIVILEGES ON inscription.* TO batman@localhost;

USE inscription; -- On se positionne sur la base 'profile'

-- Création de la table utilisateur 

CREATE TABLE IF NOT EXISTS user ( 
    id INT AUTO_INCREMENT, 
    email VARCHAR(100) NOT NULL, -- email est une chaîne de caractère de longueur variable (jusqu'à 100 caractères max)
    password VARCHAR(255) NOT NULL,  -- la valeur de cette colonne doit être renseignée
    pseudo VARCHAR(30),
    -- On indique quelle est la clé primaire : 
    PRIMARY KEY (id)
); 

-- On rajoute une contrainte d'unicité sur la colonne email
ALTER TABLE user ADD CONSTRAINT UNIQUE(email);
ALTER TABLE user ADD CONSTRAINT UNIQUE(pseudo);